use crate::components::message_list::MessageList;
use crate::App;
use tui::backend::Backend;
use tui::layout::{Alignment, Constraint, Corner, Direction, Layout};
use tui::style::{Color, Modifier, Style};
use tui::widgets::{Block, BorderType, Borders, List, ListItem, ListState, Paragraph, Row, Table};
use tui::Frame;

#[derive(Clone, PartialEq)]
pub enum View {
    Chat,
    Viewers,
    ChangeChannel,
    _UserInfo(String),
    BlockWord,
    Help,
}

pub fn draw_chat<B: Backend>(f: &mut Frame<B>, chat: &App) {
    let size = f.size();
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Min(3), Constraint::Length(3)].as_ref())
        .split(size);
    let chat_title = format!("[ {} ]", &chat.config.current_channel());
    let chat_block = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        .title(chat_title.as_ref())
        .border_style(Style::default().fg(Color::Rgb(170, 170, 170)))
        .border_type(BorderType::Plain);

    let list = MessageList::new(chat.messages.clone())
        .block(chat_block)
        .start_corner(Corner::BottomLeft);
    f.render_widget(list, chunks[0]);

    let input_title = format!("Send a message as {}", &chat.config.user.name());

    let width = chunks[1].width.max(3) - 3;
    let scroll = (chat.input.cursor() as u16).max(width) - width;
    let input = Paragraph::new(chat.input.value())
        .style(Style::default().fg(Color::White))
        .scroll((0, scroll))
        .block(
            Block::default()
                .borders(Borders::ALL)
                .border_style(Style::default().fg(Color::Rgb(170, 170, 170)))
                .title(input_title),
        );

    f.set_cursor(
        chunks[1].x + (chat.input.cursor() as u16).min(width) + 1,
        chunks[1].y + 1,
    );

    f.render_widget(input, chunks[1]);
}

pub fn draw_viewers<B: Backend>(f: &mut Frame<B>, chat: &App, viewer_list_state: &mut ListState) {
    let size = f.size();
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(100)].as_ref())
        .split(size);

    let title = format!("[ Chatters in {} ]", &chat.config.current_channel());
    let chatters_block = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        .title(title.as_ref())
        .border_style(Style::default().fg(Color::Rgb(170, 170, 170)))
        .border_type(BorderType::Plain);

    let mut items: Vec<ListItem> = Vec::new();
    for (_, c_item) in chat.viewers.iter() {
        items.push(c_item.clone());
    }

    let list = List::new(items)
        .block(chatters_block)
        .highlight_style(Style::default().add_modifier(Modifier::REVERSED));
    f.render_stateful_widget(list, chunks[0], viewer_list_state);
}

pub fn draw_blocklist<B: Backend>(f: &mut Frame<B>, chat: &App, blocklist_state: &mut ListState) {
    let size = f.size();
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Min(3), Constraint::Length(3)].as_ref())
        .split(size);
    let chat_block = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        .title("[ Blocked words ]")
        .border_style(Style::default().fg(Color::Rgb(170, 170, 170)))
        .border_type(BorderType::Plain);

    let mut items: Vec<ListItem> = Vec::new();
    for (_, b_item) in chat.blocklist.iter() {
        items.push(b_item.clone());
    }

    let list = List::new(items)
        .block(chat_block)
        .highlight_style(Style::default().add_modifier(Modifier::REVERSED));
    f.render_stateful_widget(list, chunks[0], blocklist_state);

    let width = chunks[1].width.max(3) - 3;
    let scroll = (chat.input.cursor() as u16).max(width) - width;
    let input = Paragraph::new(chat.input.value())
        .style(Style::default().fg(Color::White))
        .scroll((0, scroll))
        .block(
            Block::default()
                .borders(Borders::ALL)
                .border_style(Style::default().fg(Color::Rgb(170, 170, 170)))
                .title("Enter a word to block"),
        );

    f.set_cursor(
        chunks[1].x + (chat.input.cursor() as u16).min(width) + 1,
        chunks[1].y + 1,
    );

    f.render_widget(input, chunks[1]);
}

pub fn draw_change_channel<B: Backend>(f: &mut Frame<B>, chat: &App) {
    let mut size = f.size();
    size.height = 3;
    size.y = size.y.saturating_div(2);

    let width = size.width.max(3) - 3;
    let scroll = (chat.input.cursor() as u16).max(width) - width;
    let input = Paragraph::new(chat.input.value())
        .style(Style::default().fg(Color::White))
        .scroll((0, scroll))
        .block(
            Block::default()
                .borders(Borders::ALL)
                .border_style(Style::default().fg(Color::Rgb(170, 170, 170)))
                .title("Enter a channel name"),
        );

    f.set_cursor(
        size.x + (chat.input.cursor() as u16).min(width) + 1,
        size.y + 1,
    );
    f.render_widget(input, size);

    let second_title_rect = {
        let mut rect = size;
        rect.height = 1;
        rect.width = rect.width.saturating_sub(1);
        rect
    };

    f.render_widget(
        Paragraph::new(format!(
            "Current channel: {}",
            &chat.config.current_channel()
        ))
        .alignment(Alignment::Right),
        second_title_rect,
    );
}

pub fn draw_help<B: Backend>(f: &mut Frame<B>) {
    let size = f.size();

    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints(vec![Constraint::Min(10)].as_ref())
        .vertical_margin(15)
        .split(size);

    let block = Block::default()
        .title("[ Help ]")
        .borders(Borders::ALL)
        .border_style(Style::default().fg(Color::Rgb(170, 170, 170)));

    let table = Table::new(vec![
        Row::new(vec!["Arrow Up", "Previous."]),
        Row::new(vec!["Arrow Down", "Next."]),
        Row::new(vec!["Ctrl+q", "Exit the application."]),
        Row::new(vec!["Ctrl+c", "Change channel."]),
        Row::new(vec!["Ctrl+v", "Chatter list."]),
        Row::new(vec!["Ctrl+b", "Blocked words list."]),
        Row::new(vec!["Ctrl+n", "Normal view."]),
        Row::new(vec!["Ctrl+h", "Help."]),
        Row::new(vec!["Esc", "Exit alternate view."]),
    ])
    .header(Row::new(vec!["Keybind", "Description"]).style(Style::default().fg(Color::Blue)))
    .widths(&[Constraint::Percentage(25), Constraint::Percentage(75)])
    .style(Style::default().fg(Color::White))
    .block(block);

    f.render_widget(table, chunks[0]);
}
