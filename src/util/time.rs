use chrono::{Local, Timelike};
use dia_time::duration_to_dhms;
use std::time::Duration;

pub fn format(d: Duration) -> String {
    let (d, h, m, s) = duration_to_dhms(&d);
    let mut formatted = String::new();
    if d > 0 {
        formatted = format!("{}d", d)
    }
    if h > 0 {
        formatted += &format!("{}h", h)
    }
    if m > 0 {
        formatted += &format!("{}m", m)
    }
    if s > 0 {
        formatted += &format!("{}s", s)
    }
    formatted
}

pub fn now() -> String {
    let time_stamp = Local::now();
    format!("{:02}:{:02} ", time_stamp.hour(), time_stamp.minute())
}
