use tui::{
    style::{Color, Style},
    text::Span,
    widgets::ListItem,
};

pub fn header<'a>(name: String) -> ListItem<'a> {
    ListItem::new(Span::styled(name, Style::default().fg(Color::Blue)))
}

pub fn entry<'a>(name: String) -> ListItem<'a> {
    ListItem::new(Span::styled(name, Style::default().fg(Color::White)))
}
