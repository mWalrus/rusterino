use palette::{FromColor, Hsl, IntoColor, Saturate, Srgb};
use tui::style::Color;
use twitch_irc::message::RGBColor;

pub fn correct(c: &RGBColor) -> Color {
    // if black, brighten
    if c.r == 0 && c.g == 0 && c.b == 0 {
        return Color::Rgb(50, 50, 50);
    }

    // bring down values to between 0 and 1
    let r: f32 = c.r as f32 / 255.0;
    let g: f32 = c.g as f32 / 255.0;
    let b: f32 = c.b as f32 / 255.0;

    // convert to HSL
    let color: Hsl = Srgb::new(r, g, b).into_format().into_color();

    if color.lightness < 0.7 {
        let desaturated = color.desaturate(0.5);

        // convert back to srgb
        let new_c: Srgb = Srgb::from_color(desaturated).into_format();

        // mutliply by 255 and then make brighter by mutliplying by 1.3
        let r = ((new_c.red * 255.0) * 1.3) as u8;
        let g = ((new_c.green * 255.0) * 1.3) as u8;
        let b = ((new_c.blue * 255.0) * 1.3) as u8;

        return Color::Rgb(r, g, b);
    }

    Color::Rgb(c.r, c.g, c.b)
}
