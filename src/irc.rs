use crate::config::User;
use crate::twitch;
use crate::ui::Event;
use anyhow::Result;
use tokio::sync::mpsc::{UnboundedReceiver, UnboundedSender};
use tokio::task::JoinHandle;
use twitch_irc::{login::StaticLoginCredentials, SecureTCPTransport, TwitchIRCClient};

pub struct IRCConnection {
    current_channel: String,
    pub tx: UnboundedSender<Event>,
    pub message_rx_client: TwitchIRCClient<SecureTCPTransport, StaticLoginCredentials>,
    pub handle: JoinHandle<()>,
}

impl IRCConnection {
    pub fn new(
        user: &User,
        channel_to_join: String,
        mut rx: UnboundedReceiver<(String, String)>,
        tx: UnboundedSender<Event>,
    ) -> Result<IRCConnection> {
        let tx_cfg = twitch::config(user)?;
        let rx_cfg = twitch::config(user)?;

        let (_, message_tx_client) =
            TwitchIRCClient::<SecureTCPTransport, StaticLoginCredentials>::new(tx_cfg);
        let (mut incoming_messages, message_rx_client) =
            TwitchIRCClient::<SecureTCPTransport, StaticLoginCredentials>::new(rx_cfg);

        let tx2 = tx.clone();
        let handle = tokio::spawn(async move {
            while let Some(msg) = incoming_messages.recv().await {
                if let Err(e) = tx2.send(Event::ChatMessage(Box::new(msg))) {
                    eprintln!("{}", e);
                    return;
                }
            }
        });

        tokio::spawn(async move {
            while let Some((channel, msg)) = rx.recv().await {
                message_tx_client.privmsg(channel, msg).await.unwrap();
            }
        });

        message_rx_client.join(channel_to_join.clone());

        let conn = IRCConnection {
            current_channel: channel_to_join,
            tx,
            message_rx_client,
            handle,
        };

        Ok(conn)
    }

    pub fn send(&mut self, event_type: Event) -> Result<()> {
        self.tx.send(event_type).unwrap_or(());
        Ok(())
    }

    pub fn tx(&self) -> UnboundedSender<Event> {
        self.tx.clone()
    }

    pub fn change_channel(&mut self, new_channel: String) {
        self.message_rx_client.part(self.current_channel.clone());
        self.message_rx_client.join(new_channel.clone());
        self.current_channel = new_channel;
    }
}
