use crate::app::App;
use crate::keys::handle_key_event;
use crate::views::{self, View};
use crossterm::terminal::enable_raw_mode;
use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event as CrosstermEvent},
    execute,
    terminal::{disable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use futures::StreamExt;
use std::sync::Arc;
use std::{io, time::Duration};
use tokio::sync::mpsc::{UnboundedReceiver, UnboundedSender};
use tokio::sync::Semaphore;
use tui::{backend::CrosstermBackend, widgets::ListState, Terminal};
use twitch_irc::message::ServerMessage;

static TICK_RATE: Duration = Duration::from_millis(100);

pub enum Event {
    Input(CrosstermEvent),
    ChatMessage(Box<ServerMessage>),
    Tick,
}

pub enum UIAction {
    Quit,
    Redraw,
    None,
}

async fn feed_input(tx: UnboundedSender<Event>) {
    let mut stream = event::EventStream::new().fuse();
    while let Some(input) = stream.next().await {
        if let Ok(input) = input {
            if tx.send(Event::Input(input)).is_err() {
                // the receiver was dropped, we don't need to pump events anymore
                break;
            }
        }
        // ignore the error for now
    }
}

fn try_feed_tick(semaphore: Arc<Semaphore>, evt_tx: &UnboundedSender<Event>) {
    if let Ok(permit) = semaphore.try_acquire_owned() {
        evt_tx.send(Event::Tick).unwrap_or(());
        tokio::spawn(async move {
            tokio::time::sleep(TICK_RATE).await;
            drop(permit);
        });
    }
}

pub async fn run<'a>(
    mut app: App<'a>,
    mut rx: UnboundedReceiver<Event>,
    mut tx: UnboundedSender<(String, String)>,
) {
    enable_raw_mode().unwrap();
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture).unwrap();
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend).unwrap();
    terminal.clear().unwrap();

    // FIXME: doesn't really make sense to send a tick event to the irc-connection
    app.irc_conn.send(Event::Tick).unwrap();

    tokio::task::spawn(feed_input(app.irc_conn.tx()));

    let mut viewer_list_state = ListState::default();
    viewer_list_state.select(Some(0));

    let mut blocklist_state = ListState::default();
    blocklist_state.select(Some(0));

    // This is used to throttle updates caused by many chat messages
    let message_semaphore = Arc::new(Semaphore::new(1));

    loop {
        match rx.recv().await.unwrap() {
            Event::ChatMessage(msg) => {
                app.handle_message(msg).unwrap();
                try_feed_tick(message_semaphore.clone(), &app.irc_conn.tx);
            }
            Event::Input(event) => {
                if let CrosstermEvent::Key(key) = event {
                    let action = handle_key_event(
                        key,
                        &mut app,
                        &mut tx,
                        &mut viewer_list_state,
                        &mut blocklist_state,
                    )
                    .await;
                    match action {
                        UIAction::Quit => break,
                        UIAction::Redraw => app.irc_conn.send(Event::Tick).unwrap(),
                        UIAction::None => (),
                    }
                }
            }
            Event::Tick => {
                terminal
                    .draw(|rect| match app.view {
                        View::Chat => views::draw_chat(rect, &app),
                        View::Viewers => views::draw_viewers(rect, &app, &mut viewer_list_state),
                        View::BlockWord => views::draw_blocklist(rect, &app, &mut blocklist_state),
                        View::ChangeChannel => views::draw_change_channel(rect, &app),
                        View::Help => views::draw_help(rect),
                        _ => {}
                    })
                    .unwrap();
            }
        }
    }
    disable_raw_mode().unwrap();
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )
    .unwrap();
    terminal.show_cursor().unwrap();
    drop(app)
}
