use std::borrow::Cow;

use crate::util::{color, time};
use regex::Regex;
use tui::{
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Paragraph, Wrap},
};
use twitch_irc::message::{ClearChatAction, FollowersOnlyMode, PrivmsgMessage, RoomStateMessage};

#[derive(Clone)]
pub struct Message<'a> {
    badges: Vec<&'a str>,
    sender: Cow<'a, str>,
    content: Cow<'a, str>,
    sender_style: Option<Style>,
    text_style: Style,
    is_action: bool,
    is_system: bool,
}

impl<'a> Default for Message<'a> {
    fn default() -> Self {
        Self {
            badges: Vec::new(),
            sender: "".into(),
            content: "message".into(),
            sender_style: None,
            text_style: Style::default(),
            is_action: false,
            is_system: true,
        }
    }
}

impl<'a> Message<'a> {
    pub fn join() -> Self {
        Self {
            content: "connected".into(),
            ..Default::default()
        }
    }
    pub fn clear(action: ClearChatAction) -> Self {
        let msg = match action {
            ClearChatAction::ChatCleared => Cow::Borrowed("Chat has been cleared by a moderator."),
            ClearChatAction::UserTimedOut {
                user_login,
                timeout_length,
                ..
            } => {
                let timeout = time::format(timeout_length);
                format!("{user_login} has been timed out for {timeout}.").into()
            }
            ClearChatAction::UserBanned { user_login, .. } => {
                format!("{user_login} has been permanently banned.").into()
            }
        };

        Self {
            content: msg,
            ..Default::default()
        }
    }

    pub fn roomstate(msg: RoomStateMessage, first_join: bool) -> Vec<Self> {
        let mut roomstate_strings: Vec<String> = Vec::new();

        if let Some(toggle) = msg.emote_only {
            if toggle {
                roomstate_strings.push("This room is now in emote-only mode.".to_owned());
            } else if !toggle && !first_join {
                roomstate_strings.push("This room is no longer in emote-only mode.".to_owned());
            }
        }
        if let Some(toggle) = msg.follwers_only {
            match toggle {
                FollowersOnlyMode::Enabled(d) => {
                    let time = time::format(d);
                    let fmt = format!("This room is now in {time} followers-only mode.");
                    roomstate_strings.push(fmt);
                }
                FollowersOnlyMode::Disabled => {
                    if !first_join {
                        roomstate_strings
                            .push("This room is no longer in followers-only mode.".to_owned());
                    }
                }
            }
        }
        if let Some(toggle) = msg.r9k {
            if toggle {
                roomstate_strings.push("This room is now in unique-chat mode.".to_owned());
            } else if !toggle && !first_join {
                roomstate_strings.push("This room is no longer in unique-chat mode.".to_owned());
            }
        }
        if let Some(d) = msg.slow_mode {
            if d.as_secs() == 0 && !first_join {
                roomstate_strings.push("This room is no longer in slow mode.".to_owned());
            } else if d.as_secs() > 0 {
                let time = time::format(d);
                let fmt = format!("This room is in {time} slow mode.");
                roomstate_strings.push(fmt);
            }
        }
        if let Some(toggle) = msg.subscribers_only {
            if toggle {
                roomstate_strings.push("This room is now in subscribers-only mode.".to_owned());
            } else if !toggle && !first_join {
                roomstate_strings
                    .push("This room is no longer in subscribers-only mode.".to_owned());
            }
        }
        let mut messages: Vec<Message<'a>> = Vec::new();
        for roomstate in roomstate_strings {
            messages.push(Self {
                content: roomstate.into(),
                ..Default::default()
            })
        }
        messages
    }
    pub fn privmsg(msg: PrivmsgMessage, user_name: &str, user_regex: &Regex) -> Self {
        let sender_style = match &msg.name_color {
            Some(c) => {
                let c = color::correct(c);
                Style::default().fg(c)
            }
            None => Style::default().fg(Color::Rgb(150, 150, 150)),
        };

        // msg highlighting on @
        let user_name = user_name.to_lowercase();
        let mut text_style = Style::default();
        if user_regex.is_match(&msg.message_text.to_lowercase())
            && msg.sender.name.to_lowercase() != user_name
        {
            text_style = text_style.bg(Color::Rgb(87, 15, 58))
        }

        let mut badges_collection: Vec<&str> = Vec::new();
        for badge in msg.badges.iter() {
            let badge = match badge.name.as_str() {
                "broadcaster" => "🟥",
                "moderator" => "🟩",
                "vip" => "🔷",
                "subscriber" => "🌟",
                "founder" => "🌠",
                "staff" => "🔧",
                _ => "",
            };
            badges_collection.push(badge);
        }

        Self {
            badges: badges_collection,
            sender: msg.sender.name.into(),
            content: msg.message_text.into(),
            sender_style: Some(sender_style),
            text_style,
            is_action: msg.is_action,
            is_system: false,
        }
    }
    pub fn height(&self, parent_width: usize) -> usize {
        let mut content_width =
            6 + self.badges.len() + self.sender.len() + self.content.chars().count();
        if self.is_action {
            content_width += 1; // is_action separates name from msg content with only 1 whitespace character
        } else {
            content_width += 2; // if its not it separates with ": ", which has len of 2
        }

        let remainder = content_width % parent_width;
        let has_remainder = remainder > 0;
        let height = content_width / parent_width;

        if !has_remainder && height == 0 {
            1
        } else if !has_remainder && height > 0 {
            height
        } else {
            height + 1
        }
    }
    pub fn to_paragraph(&self) -> Paragraph {
        let sender_style = self.sender_style.unwrap_or_default();
        let system_color = Color::Rgb(150, 150, 150);

        let text_style = if self.is_action {
            self.text_style.fg(sender_style.fg.unwrap_or(Color::White))
        } else if self.is_system {
            self.text_style.fg(system_color)
        } else {
            self.text_style
        };

        let mut formatted = Spans(vec![
            Span::styled(time::now(), Style::default().fg(system_color)),
            Span::raw(self.badges.join("")),
            Span::styled(
                self.sender.clone(),
                sender_style.add_modifier(Modifier::BOLD),
            ),
        ]);

        if !self.is_action && !self.is_system {
            formatted.0.push(Span::styled(": ", sender_style));
        } else if self.is_action && !self.is_system {
            formatted.0.push(Span::raw(" "));
        }

        formatted
            .0
            .push(Span::styled(self.content.clone(), text_style));

        Paragraph::new(formatted).wrap(Wrap { trim: true })
    }
}
