use std::collections::VecDeque;

#[derive(Clone)]
pub struct MessageHistory {
    pub index: usize,
    pub messages: VecDeque<String>,
}

impl Default for MessageHistory {
    fn default() -> Self {
        MessageHistory {
            index: 0,
            messages: VecDeque::with_capacity(100),
        }
    }
}

impl MessageHistory {
    pub fn next(&mut self) -> String {
        if (self.index < self.messages.capacity()) && (self.index < self.messages.len()) {
            self.index += 1;
        }
        match self.messages.get(self.index) {
            Some(m) => m.to_owned(),
            None => String::new(),
        }
    }
    pub fn previous(&mut self) -> String {
        if self.index > 0 {
            self.index -= 1;
        }
        match self.messages.get(self.index) {
            Some(m) => m.to_owned(),
            None => String::new(),
        }
    }
    pub fn push(&mut self, msg: String) {
        if msg.is_empty() {
            return;
        }
        match self.messages.front() {
            Some(m) => {
                if msg.ne(m) {
                    self.messages.push_back(msg);
                    self.reset_index();
                }
            }
            None => {
                self.messages.push_back(msg);
                self.reset_index();
            }
        }
    }
    fn reset_index(&mut self) {
        self.index = self.messages.len();
    }
}
