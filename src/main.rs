mod app;
mod components;
mod config;
mod handlers;
mod irc;
mod keys;
mod twitch;
mod ui;
mod util;
mod views;

use anyhow::Result;
use app::App;
use config::Config;
use irc::IRCConnection;
use tokio::sync::mpsc;
use ui::Event;

//#[macro_use] extern crate log;
//use simplelog::*;
//use std::fs::File;

#[actix_web::main]
async fn main() -> Result<()> {
    // logging
    //CombinedLogger::init(
    //vec![WriteLogger::new(LevelFilter::Info, Config::default(), File::create("message_dimensions.log").unwrap())]
    //).unwrap();

    // Read settings
    let config = Config::read()?;
    let user = config.user();

    // event handler (tick, keypress, incoming chat msg)
    let (evt_tx, evt_rx) = mpsc::unbounded_channel::<Event>();
    // sending message from the input box to the irc client
    let (ui_msg_tx, ui_msg_rx) = mpsc::unbounded_channel::<(String, String)>();

    let irc_conn = IRCConnection::new(&user, config.current_channel(), ui_msg_rx, evt_tx)?;

    let app = App::new(config, irc_conn).await;

    ui::run(app, evt_rx, ui_msg_tx).await;

    Ok(())
}
