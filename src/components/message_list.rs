use crate::handlers::message::Message;
use std::collections::VecDeque;
use tui::layout::{Corner, Rect};
use tui::style::Style;
use tui::widgets::{Block, Widget};

pub struct MessageList<'a> {
    block: Option<Block<'a>>,
    items: VecDeque<Message<'a>>,
    style: Style,
    start_corner: Corner,
}

impl<'a> MessageList<'a> {
    pub fn new(messages: VecDeque<Message<'a>>) -> MessageList<'a> {
        MessageList {
            block: None,
            style: Style::default(),
            items: messages,
            start_corner: Corner::BottomLeft,
        }
    }

    pub fn block(mut self, block: Block<'a>) -> MessageList<'a> {
        self.block = Some(block);
        self
    }

    pub fn start_corner(mut self, start_corner: Corner) -> MessageList<'a> {
        self.start_corner = start_corner;
        self
    }

    pub fn item_count(&self, max_height: usize, max_width: usize) -> usize {
        let mut end = 0;
        let mut height = 0;
        for item in self.items.iter() {
            let item_height = item.height(max_width);
            if height + item_height > max_height {
                break;
            }
            height += item_height;
            end += 1;
        }
        end
    }
}

impl<'a> Widget for MessageList<'a> {
    fn render(mut self, area: tui::layout::Rect, buf: &mut tui::buffer::Buffer) {
        // give the buffer its style
        buf.set_style(area, self.style);
        // get the area of the parent block
        let list_area = match self.block.take() {
            Some(b) => {
                let inner_area = b.inner(area);
                b.render(area, buf);
                inner_area
            }
            None => area,
        };
        // skip rendering the items if the size is too small
        if list_area.width < 1 || list_area.height < 1 {
            return;
        }
        // skip rendering if there are no items in the list
        if self.items.is_empty() {
            return;
        }

        // get the height and width as usize
        let list_height = list_area.height as usize;
        let list_width = list_area.width as usize;

        // get the number of items that could fit in the list
        let item_count = self.item_count(list_height, list_width);

        let mut current_height = 0;
        for (_, item) in self.items.iter_mut().enumerate().take(item_count) {
            let item_height = item.height(list_width) as u16;
            let (x, y) = match self.start_corner {
                Corner::BottomLeft => {
                    current_height += item_height;
                    (list_area.left(), list_area.bottom() - current_height)
                }
                _ => {
                    let pos = (list_area.left(), list_area.top() + current_height);
                    current_height += item_height;
                    pos
                }
            };
            item.to_paragraph().render(
                Rect {
                    x,
                    y,
                    width: list_width as u16,
                    height: item_height as u16,
                },
                buf,
            )
        }
    }
}
