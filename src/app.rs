use crate::config::Config;
use crate::handlers::message::Message;
use crate::handlers::message_history::MessageHistory;
use crate::irc::IRCConnection;
use crate::twitch;
use crate::util::item;
use crate::views::View;
use anyhow::Result;
use regex::Regex;
use std::collections::VecDeque;
use std::slice::Iter;
use tui::widgets::ListItem;
use tui_input::Input;
use twitch_irc::message::ServerMessage;

pub struct App<'a> {
    pub config: Config,
    pub is_first_join: bool,
    pub messages: VecDeque<Message<'a>>,
    pub input: Input,
    pub message_history: MessageHistory,
    pub viewers: Vec<(String, ListItem<'a>)>,
    pub view: View,
    pub blocklist: BlockList<'a>,
    pub user_regex: Regex,
    pub irc_conn: IRCConnection,
}

pub const MESSAGE_CAPACITY: usize = 50;

pub struct BlockList<'a>(Vec<(String, ListItem<'a>)>);

impl From<Vec<String>> for BlockList<'_> {
    fn from(bl: Vec<String>) -> Self {
        let mut block_list: Vec<(String, ListItem<'_>)> = Vec::new();
        for word in bl {
            block_list.push((word.to_owned(), item::entry(word)));
        }
        BlockList(block_list)
    }
}

impl BlockList<'_> {
    fn contains(&self, text: &str) -> bool {
        for (word, _) in &self.0 {
            if text.contains(word) {
                return true;
            }
        }
        false
    }
    fn push(&mut self, word: String) {
        self.0.push((word.clone(), item::entry(word)));
    }
    pub fn remove(&mut self, index: usize) {
        self.0.remove(index);
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }
    pub fn iter(&self) -> Iter<'_, (String, ListItem<'_>)> {
        self.0.iter()
    }
}

impl<'a> App<'a> {
    pub async fn new(config: Config, irc_conn: IRCConnection) -> App<'a> {
        let viewers = build_viewer_list(&config.current_channel).await;

        let blocklist = BlockList::from(config.blocked_words());

        let regex_string = format!(r"(^|\s)@?{},?(\s|$)", config.user.name());
        let user_regex = Regex::new(&regex_string).unwrap();

        App {
            config,
            viewers,
            user_regex,
            irc_conn,
            blocklist,
            messages: VecDeque::with_capacity(MESSAGE_CAPACITY),
            input: Input::default(),
            is_first_join: true,
            message_history: MessageHistory::default(),
            view: View::Chat,
        }
    }

    pub fn handle_message(&mut self, msg: Box<ServerMessage>) -> Result<()> {
        match *msg {
            ServerMessage::Privmsg(m) => {
                if self.blocklist.contains(&m.message_text) {
                    return Ok(());
                }
                let data = Message::privmsg(m, &self.config.user.name(), &self.user_regex);
                self.messages.push_front(data);
            }
            ServerMessage::Join(_) => {
                let data = Message::join();
                self.messages.push_front(data);
            }
            ServerMessage::ClearChat(m) => {
                let data = Message::clear(m.action);
                self.messages.push_front(data);
            }
            ServerMessage::RoomState(m) => {
                let messages = Message::roomstate(m, self.is_first_join);
                if !messages.is_empty() {
                    for message in messages {
                        self.messages.push_front(message);
                    }
                    if self.is_first_join {
                        self.is_first_join = false;
                    }
                }
            }
            _ => {}
        };
        Ok(())
    }

    pub async fn change_channel(&mut self, new_channel: String) -> Result<()> {
        self.viewers = build_viewer_list(&new_channel).await;
        self.irc_conn.change_channel(new_channel.clone());
        self.config.change_channel(new_channel);
        self.messages = VecDeque::with_capacity(MESSAGE_CAPACITY);
        Ok(())
    }

    pub fn add_blocked_word(&mut self, word: String) {
        self.blocklist.push(word.clone());
        self.config.add_blocked_word(word);
    }
}

async fn build_viewer_list<'a>(current_channel: &str) -> Vec<(String, ListItem<'a>)> {
    let mut chatters: Vec<(String, ListItem<'a>)> = Vec::new();

    let (count, chatters_map) = twitch::chatters(current_channel).await.unwrap();

    let count_string = format!("chatter count: {}", &count);
    chatters.push(("+c".to_string(), item::entry(count_string)));
    chatters.push(("+e".to_string(), item::entry(String::default())));

    for category in chatters_map.into_iter() {
        let items = category.1.as_array().unwrap();
        if !items.is_empty() {
            chatters.push(("+h".to_string(), item::header(category.0)));
            for item in items {
                let item = item.as_str().unwrap_or_default().to_owned();
                chatters.push((item.clone(), item::entry(item)))
            }
            chatters.push(("+e".to_string(), item::entry(String::default())));
        }
    }

    chatters
}
