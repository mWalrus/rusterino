use crate::app::App;
use crate::ui::UIAction;
use crate::views::View;
use crossterm::event::{Event as CrosstermEvent, KeyEvent};
use lazy_static::lazy_static;
use tokio::sync::mpsc::UnboundedSender;
use tui::widgets::ListState;
use tui_input::{backend::crossterm as input_backend, Input};

pub mod key_map;
use key_map::KeyMap;

lazy_static! {
    static ref KEY_MAP: KeyMap = KeyMap::default();
}

pub async fn handle_key_event(
    key: KeyEvent,
    app: &mut App<'_>,
    tx: &mut UnboundedSender<(String, String)>,
    cls: &mut ListState,
    bls: &mut ListState,
) -> UIAction {
    match app.view {
        View::Chat => {
            if key == KEY_MAP.prev {
                app.input = Input::default().with_value(app.message_history.previous());
            } else if key == KEY_MAP.next {
                app.input = Input::default().with_value(app.message_history.next());
            } else if key == KEY_MAP.tab {
                let input: String = (*app.input.value()).into();
                let search_term: &str = input.split(' ').last().unwrap_or_default();
                for (viewer, _) in app.viewers.iter() {
                    if viewer.starts_with(search_term) && !viewer.starts_with('+') {
                        let input = if input.contains(' ') {
                            let before = input.rsplit_once(' ').unwrap_or_default().0;
                            format!("{before} {viewer} ")
                        } else {
                            viewer.clone() + " "
                        };
                        app.input = Input::default().with_value(input);
                    }
                }
            } else if key == KEY_MAP.send {
                let msg: String = (*app.input.value()).into();
                app.message_history.push(msg.clone());
                app.input = Input::default();
                if let Err(err) = tx.send((app.config.current_channel(), msg)) {
                    panic!("{err}");
                }
            } else if key == KEY_MAP.send_retain_input {
                let msg: String = (*app.input.value()).into();
                app.message_history.push(msg.clone());
                if let Err(err) = tx.send((app.config.current_channel(), msg)) {
                    panic!("{err}");
                }
                return UIAction::None;
            } else if key == KEY_MAP.viewers {
                app.view = View::Viewers;
            } else if key == KEY_MAP.blocklist {
                app.view = View::BlockWord;
            } else if key == KEY_MAP.change_channel {
                app.view = View::ChangeChannel;
            } else if key == KEY_MAP.help {
                app.view = View::Help;
            } else if key == KEY_MAP.quit {
                return UIAction::Quit;
            } else {
                // typing
                input_backend::to_input_request(CrosstermEvent::Key(key))
                    .and_then(|r| app.input.handle(r));
            }
            return UIAction::Redraw;
        }
        View::Viewers => {
            // FIXME: implement bind for larger steps
            if key == KEY_MAP.prev {
                cls.select(decrement_selection(app, 1, cls.selected()));
            } else if key == KEY_MAP.next {
                cls.select(increment_selection(app, 1, cls.selected()));
            } else if key == KEY_MAP.back || key == KEY_MAP.chat {
                app.view = View::Chat;
            } else if key == KEY_MAP.quit {
                return UIAction::Quit;
            } else {
                return UIAction::None;
            }
            return UIAction::Redraw;
        }
        View::BlockWord => {
            if key == KEY_MAP.prev {
                if let Some(sel) = bls.selected() {
                    if sel.ge(&(app.blocklist.len() - 1)) {
                        bls.select(Some(0));
                    } else {
                        bls.select(Some(sel + 1));
                    }
                }
            } else if key == KEY_MAP.next {
                if let Some(sel) = bls.selected() {
                    if sel > 0 {
                        bls.select(Some(sel - 1));
                    } else {
                        bls.select(Some(app.blocklist.len() - 1));
                    }
                }
            } else if key == KEY_MAP.blocklist_delete {
                let sel = bls.selected();
                if let Some(s) = sel {
                    app.blocklist.remove(s);
                    bls.select(Some(s.saturating_sub(1)));
                }
            } else if key == KEY_MAP.back || key == KEY_MAP.chat {
                app.view = View::Chat;
            } else if key == KEY_MAP.quit {
                return UIAction::Quit;
            } else {
                input_backend::to_input_request(CrosstermEvent::Key(key))
                    .and_then(|r| app.input.handle(r));
            }
            return UIAction::Redraw;
        }
        View::ChangeChannel => {
            if key == KEY_MAP.back || key == KEY_MAP.chat {
                app.view = View::Chat;
            } else if key == KEY_MAP.send {
                let new_channel: String = (*app.input.value()).into();
                app.change_channel(new_channel).await.unwrap();
                app.input = Input::default();
                app.view = View::Chat;
                app.is_first_join = true;
            } else if key == KEY_MAP.quit {
                return UIAction::Quit;
            } else {
                input_backend::to_input_request(CrosstermEvent::Key(key))
                    .and_then(|r| app.input.handle(r));
            }
            return UIAction::Redraw;
        }
        View::Help => {
            if key == KEY_MAP.back || key == KEY_MAP.chat {
                app.view = View::Chat;
                return UIAction::Redraw;
            } else if key == KEY_MAP.quit {
                return UIAction::Quit;
            } else {
                return UIAction::None;
            }
        }
        _ => {}
    }
    UIAction::None
}

fn decrement_selection(app: &App, step: usize, sel: Option<usize>) -> Option<usize> {
    if let Some(selected) = sel {
        return if selected > step {
            let mut next = selected - step;
            let (i_name, _) = app.viewers.get(next).unwrap();
            if i_name.starts_with("+h") {
                next -= 2;
            } else if i_name.starts_with("+e") {
                next -= 1;
            }
            Some(next)
        } else {
            Some(app.viewers.len() - 1)
        };
    }
    None
}

fn increment_selection(app: &App, step: usize, sel: Option<usize>) -> Option<usize> {
    if let Some(selected) = sel {
        return if selected.ge(&(app.viewers.len() - step)) {
            Some(0)
        } else {
            let next = selected + step;
            let (i_name, _) = app.viewers.get(next).unwrap();
            if i_name.starts_with("+e") {
                Some(next + 2)
            } else {
                Some(next + 1)
            }
        };
    }
    None
}
