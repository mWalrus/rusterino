use crossterm::event::{KeyCode, KeyEvent, KeyModifiers};

pub struct KeyMap {
    pub chat: KeyEvent,
    pub viewers: KeyEvent,
    pub blocklist: KeyEvent,
    pub blocklist_delete: KeyEvent,
    pub change_channel: KeyEvent,
    pub help: KeyEvent,
    pub prev: KeyEvent,
    pub next: KeyEvent,
    pub send: KeyEvent,
    pub send_retain_input: KeyEvent,
    pub tab: KeyEvent,
    pub back: KeyEvent,
    pub quit: KeyEvent,
}

impl Default for KeyMap {
    fn default() -> Self {
        Self {
            chat: KeyEvent {
                code: KeyCode::Char('n'),
                modifiers: KeyModifiers::CONTROL,
            },
            viewers: KeyEvent {
                code: KeyCode::Char('v'),
                modifiers: KeyModifiers::CONTROL,
            },
            blocklist: KeyEvent {
                code: KeyCode::Char('b'),
                modifiers: KeyModifiers::CONTROL,
            },
            blocklist_delete: KeyEvent {
                code: KeyCode::Char('d'),
                modifiers: KeyModifiers::NONE,
            },
            change_channel: KeyEvent {
                code: KeyCode::Char('c'),
                modifiers: KeyModifiers::CONTROL,
            },
            help: KeyEvent {
                code: KeyCode::Char('h'),
                modifiers: KeyModifiers::CONTROL,
            },
            prev: KeyEvent {
                code: KeyCode::Up,
                modifiers: KeyModifiers::NONE,
            },
            next: KeyEvent {
                code: KeyCode::Down,
                modifiers: KeyModifiers::NONE,
            },
            send: KeyEvent {
                code: KeyCode::Enter,
                modifiers: KeyModifiers::NONE,
            },
            send_retain_input: KeyEvent {
                code: KeyCode::Enter,
                modifiers: KeyModifiers::ALT,
            },
            tab: KeyEvent {
                code: KeyCode::Tab,
                modifiers: KeyModifiers::NONE,
            },
            back: KeyEvent {
                code: KeyCode::Esc,
                modifiers: KeyModifiers::NONE,
            },
            quit: KeyEvent {
                code: KeyCode::Char('q'),
                modifiers: KeyModifiers::CONTROL,
            },
        }
    }
}
