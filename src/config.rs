use anyhow::Result;
use confy;
use console::Term;
use dialoguer::{theme::ColorfulTheme, Confirm, Input};
use serde::{Deserialize, Serialize};
use webbrowser;

#[derive(Serialize, Deserialize, Clone, Default, PartialEq)]
pub struct User {
    pub name: String,
    pub id: i32,
    pub client_id: String,
    pub oauth_token: String,
}

#[derive(Serialize, Deserialize, Clone, Default, PartialEq)]
pub struct Config {
    pub current_channel: String,
    blocked_words: Vec<String>,
    pub user: User,
}

impl Drop for Config {
    fn drop(&mut self) {
        self.save().unwrap();
    }
}

impl Config {
    pub fn read() -> Result<Config> {
        let settings = confy::load::<Config>("rusterino")?;
        if settings == Config::default() {
            let user = User::new();
            let settings = Config {
                current_channel: user.name.clone(),
                blocked_words: Vec::new(),
                user,
            };
            settings.save()?;
            return Ok(settings);
        }
        Ok(settings)
    }

    pub fn _update_user(mut self) -> Result<()> {
        self.user = User::new();
        Ok(())
    }

    pub fn user(&self) -> User {
        self.user.clone()
    }

    pub fn current_channel(&self) -> String {
        self.current_channel.to_string()
    }

    pub fn change_channel(&mut self, new_channel: String) {
        self.current_channel = new_channel;
        self.save().unwrap();
    }

    pub fn blocked_words(&self) -> Vec<String> {
        self.blocked_words.clone()
    }

    pub fn add_blocked_word(&mut self, word: String) {
        self.blocked_words.push(word);
        self.save().unwrap();
    }

    pub fn save(&self) -> Result<()> {
        confy::store("rusterino", self)?;
        Ok(())
    }
}

impl User {
    fn new() -> Self {
        if Confirm::with_theme(&ColorfulTheme::default())
            .with_prompt("Open login page in browser?")
            .default(true)
            .interact()
            .unwrap()
        {
            webbrowser::open("https://rusterino.waalrus.xyz/login").unwrap();
            // handle callback user data from twitch
            let user_data: String = Input::with_theme(&ColorfulTheme::default())
                .with_prompt("Paste login info here")
                .interact_on(&Term::stderr())
                .unwrap();
            let user_data: Vec<&str> = user_data.split(';').collect();

            let name = user_data[0].to_string();
            User {
                name: name.clone(),
                id: user_data[1].to_string().parse::<i32>().unwrap(),
                client_id: user_data[2].to_string(),
                oauth_token: user_data[3].to_string(),
            }
        } else {
            User::default()
        }
    }
    pub fn name(&self) -> String {
        self.name.clone()
    }
}
