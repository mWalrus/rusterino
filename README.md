# RUSTERINO
A TUI twitch chat client written in rust.
![Chat view](https://i.imgur.com/Qgfzf59.png)

## Setting it up
After installing the application you need to set up the twitch oauth callback. <br>
For now the setup process involves setting up an application in your twitch dev console which points to localhost.
1. Go to `https://dev.twitch.tv/console` and log in
2. Under applications click the `Register your application` button.
3. Give it a name and set the `OAuth Redirect URLs` to `http://localhost:<any-port-you-want>/user-login`
4. Save and start the program
5. When asked for port, specify the port you set in the `Oauth Redirect URLs`, otherwise authenticating wont work.
